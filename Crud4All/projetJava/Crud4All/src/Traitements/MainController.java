package Traitements;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;


public class MainController implements Initializable {
    //Main
    @FXML
    private Button addButton;
    @FXML
    private Button departmentButton;
    @FXML
    private Button professorsButton;
    @FXML
    private TextField searchField;
    @FXML
    private AnchorPane switchPane;
    @FXML
    private Text switchTitle;
    @FXML
    private VBox vBoxButton;

    @FXML
    private Button ajouterButton;

    //addProf
    @FXML
    private TextField adresseField;
    @FXML
    private Button ajouterButton1;
    @FXML
    private Button annulerButton1;
    @FXML
    private TextField cinField;
    @FXML
    private DatePicker dateField;
    @FXML
    private TextField departementField;
    @FXML
    private TextField emailField;
    @FXML
    private Label erreur;
    @FXML
    private TextField nomField;
    @FXML
    private TextField prenomField;
    @FXML
    private TextField telephoneField;

    //profInfos
    @FXML
    private TextField adresseField1;
    @FXML
    private TextField cinField1;
    @FXML
    private DatePicker dateField1;
    @FXML
    private TextField departementField1;
    @FXML
    private TextField emailField1;
    @FXML
    private TextField nomField1;
    @FXML
    private TextField prenomField1;
    @FXML
    private TextField telephoneField1;
    @FXML
    private TextField nomField2;

    @FXML
    private TableColumn<Professeur, String> adresseColumn;
    @FXML
    private TableColumn<Professeur, String> cinColumn;
    @FXML
    private TableColumn<Professeur, String>  emailColumn;
    @FXML
    private TableColumn<Professeur, String>  nomColumn;
    @FXML
    private TableColumn<Professeur, String>  prenomColumn;
    @FXML
    private TableView<Professeur> tableView;
    @FXML
    private TableColumn<Professeur, String> telColumn;



    ObservableList<Professeur> professeurObservableList = FXCollections.observableArrayList();
    ObservableList<Departement> departementObservableList = FXCollections.observableArrayList();
    MetierImpl dbConnect = new MetierImpl();
    List<Button> buttonList= new ArrayList<>();
    Button clickedButton= new Button();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try{
            departementObservableList = dbConnect.getAllDepartements();
            professeurObservableList = dbConnect.getAllProfessors();
            ShowProfs();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    //main
    //show departements list
    @FXML
    public void ShowDeparts() {
        departementObservableList = dbConnect.getAllDepartements();
        professorsButton.getStyleClass().remove("pressedMainButton");
        departmentButton.getStyleClass().remove("pressedMainButton");
        departmentButton.getStyleClass().add("pressedMainButton");

        switchPane.getChildren().clear();
        switchTitle.setText("Departements");

        addDepartsButtons(departementObservableList);

        addButton.setOnAction((ActionEvent e)->{
            FxmlLoader object = new FxmlLoader();
            Pane view = object.getPane("AddDepart");
            switchPane.getChildren().clear();
            switchPane.getChildren().setAll(view);
        });

        FilteredList<Departement> filterData = new FilteredList<>(departementObservableList, b-> true);

        searchField.textProperty().addListener((observable, oldVal, newVal)->{
            ObservableList<Departement> filteredList = FXCollections.observableArrayList();
            if(newVal.isEmpty() || newVal == null || newVal ==""){
                addDepartsButtons(departementObservableList);
            }
            else {
                String search = newVal.toLowerCase();
                for (Departement departement : departementObservableList) {
                    String filterText = departement.getNom();
                    if (filterText.toLowerCase().contains(search)) {
                        filteredList.add(departement);
                    }
                }
                addDepartsButtons(filteredList);
            }
        });

    }

    //show profs list
    @FXML
    void ShowProfs() {
        professeurObservableList = dbConnect.getAllProfessors();
        departmentButton.getStyleClass().remove("pressedMainButton");
        professorsButton.getStyleClass().remove("pressedMainButton");
        professorsButton.getStyleClass().add("pressedMainButton");

        switchPane.getChildren().clear();
        switchTitle.setText("Professeurs");
        addProfsButtons(professeurObservableList);

        addButton.setOnAction((ActionEvent e)->{
            FxmlLoader object = new FxmlLoader();
            Pane view = object.getPane("AddProf");
            switchPane.getChildren().clear();
            switchPane.getChildren().setAll(view);
        });

        FilteredList<Professeur> filterData = new FilteredList<>(professeurObservableList, b-> true);

        searchField.textProperty().addListener((observable, oldVal, newVal)->{
            ObservableList<Professeur> filteredList = FXCollections.observableArrayList();
                if(newVal.isEmpty() || newVal == null || newVal ==""){
                    addProfsButtons(professeurObservableList);
                }
                else {
                    String search = newVal.toLowerCase();
                    for (Professeur p : professeurObservableList) {
                        String filterText = p.getNom() + " " + p.getPrenom();
                        if (filterText.toLowerCase().contains(search)) {
                            filteredList.add(p);
                        }
                    }
                    addProfsButtons(filteredList);
                }
            });


    }


    public void addProfsButtons(ObservableList<Professeur> list){
        buttonList.clear();
        for(Professeur p:list){
            Button b = new Button(p.getNom()+" "+p.getPrenom());
            b.getStyleClass().add("itemButton");
            b.setUserData(p.getCin());

            /////
            b.setOnAction(event -> {
                for(Button d:buttonList){
                    d.getStyleClass().remove("pressedButton");
                }
                b.getStyleClass().add("pressedButton");
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../Presentation/ShowProfInfos.fxml"));
                AnchorPane view = null;
                try {
                    view = loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                MainController controller = loader.getController();
                controller.showInfos((String) b.getUserData(), event);
                switchPane.getChildren().clear();
                switchPane.getChildren().setAll(view);
            });

            buttonList.add(b);
        }
        vBoxButton.getChildren().clear();
        vBoxButton.getChildren().addAll(buttonList);
    }

    public void addDepartsButtons(ObservableList<Departement> list){
        buttonList.clear();
        for(Departement departement:list){
            Button b = new Button(departement.getNom());
            b.getStyleClass().add("itemButton");
            b.setUserData(departement.getId_depart());

            /////
            b.setOnAction(event -> {
                for(Button d:buttonList){
                    d.getStyleClass().remove("pressedButton");
                }
                b.getStyleClass().add("pressedButton");
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../Presentation/showDepartementProfs.fxml"));
                AnchorPane view = null;
                try {
                    view = loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                MainController controller = loader.getController();
                controller.showInfosDepart( event);
                switchPane.getChildren().clear();
                switchPane.getChildren().setAll(view);
            });

            buttonList.add(b);
        }
        vBoxButton.getChildren().clear();
        vBoxButton.getChildren().addAll(buttonList);
    }

    //ajouter Prof
    @FXML
    public void ajouterProf() throws IOException {
        erreur.setVisible(false);
        Professeur pr = new Professeur();
        try {
            pr.setNom(nomField.getText());
            pr.setPrenom(prenomField.getText());
            pr.setAdresse(adresseField.getText());
            pr.setEmail(emailField.getText());
            pr.setCin(cinField.getText());
            pr.setTelephone(telephoneField.getText());
            pr.setDate_recrutement(dateField.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        }catch(Exception ee){

        }
        Departement dep = null;
        for(Departement d:departementObservableList){
            if(departementField.getText().equals(d.getNom())){
                dep = d;
                pr.setDepartement(d);
            }
        }
        if(pr.getNom().isEmpty() || pr.getPrenom().isEmpty() || pr.getCin().isEmpty() || pr.getDate_recrutement().isEmpty() || pr.getAdresse().isEmpty() || pr.getEmail().isEmpty() || pr.getTelephone().isEmpty()){
            erreur.setText("Tous les champs sont obligatoires !");
            erreur.setVisible(true);
        }
        else if(pr.getDepartement()==null){
            erreur.setText("Departement n'existe pas !");
            erreur.setVisible(true);
        }
        else{
            dbConnect.addProfessor(pr,dep.getId_depart());
            dbConnect.addProfToDepart(pr, dep);
            professeurObservableList.add(pr);
            //refreshPage(annulerButton1);
        }
    }
    //Annuler ajout
    @FXML
    void annulerAjoutProf() {
        refreshPage(annulerButton1);
    }

    @FXML
    public void ajouterDepartement(){
        erreur.setVisible(false);
        Departement dept = new Departement();
        if(nomField.getText().isEmpty()){
            erreur.setText("Le champ nom est obligatoire !");
            erreur.setVisible(true);
        }
        else{
            dept.setNom(nomField.getText());
            dbConnect.addDepartement(dept);
            departementObservableList.add(dept);
            //refreshPage(ajouterButton);
        }
    }

    @FXML
    public void annulerAjoutDepart(){
        refreshPage(ajouterButton);
    }

    public void refreshPage(Button button){
        Stage stage = (Stage) button.getScene().getWindow();
        stage.close();
        Platform.runLater( () -> {
            try {
                new Main().start( new Stage() );
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }



    public void showInfos(String cin, ActionEvent e){
        clickedButton = (Button) e.getSource();
        System.out.println(clickedButton.getUserData());
        Professeur pr = dbConnect.getProfessorsById(cin);
        nomField1.setText(pr.getNom());
        prenomField1.setText(pr.getPrenom());
        emailField1.setText(pr.getEmail());
        adresseField1.setText(pr.getAdresse());
        telephoneField1.setText(pr.getTelephone());
        dateField1.setValue(LOCAL_DATE(pr.getDate_recrutement()));
        cinField1.setText(pr.getCin());
        departementField1.setText(pr.getDepartement().getNom());
    }

    @FXML
    public void modifierProf(ActionEvent event){
        System.out.println("modifier ...");
        Professeur pr = new Professeur();
            pr.setNom(nomField1.getText());
            pr.setPrenom(prenomField1.getText());
            pr.setAdresse(adresseField1.getText());
            pr.setEmail(emailField1.getText());
            pr.setCin(cinField1.getText());
            pr.setTelephone(telephoneField1.getText());
            pr.setDate_recrutement(dateField1.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

        Professeur prof = dbConnect.getProfessorsById((String) clickedButton.getUserData());
        System.out.println(prof.getNom());
        Departement dep = null;
        for(Departement d:departementObservableList){
            if(departementField1.getText().equals(d.getNom())){
                dep = d;
                pr.setDepartement(d);
                break;
            }
        }
        if(pr.getNom().isEmpty() || pr.getPrenom().isEmpty() || pr.getCin().isEmpty() || pr.getDate_recrutement().isEmpty() || pr.getAdresse().isEmpty() || pr.getEmail().isEmpty() || pr.getTelephone().isEmpty()){
            System.out.println("a");
            if(pr.getNom().isEmpty()){
                pr.setNom(prof.getNom());
            }
            if(pr.getPrenom().isEmpty()){
                pr.setPrenom(prof.getPrenom());
            }
            if(pr.getCin().isEmpty()){
                pr.setCin(prof.getCin());
            }if(pr.getAdresse().isEmpty()){
                pr.setAdresse(prof.getAdresse());
            }
            if(pr.getTelephone().isEmpty()){
                pr.setTelephone(prof.getTelephone());
            }
            if(pr.getEmail().isEmpty()){
                pr.setEmail(prof.getEmail());
            }
            if(pr.getDate_recrutement().isEmpty()){
                pr.setDate_recrutement(prof.getDate_recrutement());
            }
        }
        else if(pr.getDepartement()==null || departementField1.getText().isEmpty()){
            System.out.println("b");
            pr.setDepartement(prof.getDepartement());
        }
        else{
            dbConnect.modifyProfessor(prof, pr);
            System.out.println("modified");
        }
    }

    @FXML
    public void supprimerProf(ActionEvent event){
        System.out.println("delete");
        Professeur pr = dbConnect.getProfessorsById((String) clickedButton.getUserData());
        dbConnect.removeProfessor(pr);
    }

    @FXML
    public void showInfosDepart(ActionEvent e){
        nomColumn.setCellValueFactory(new PropertyValueFactory<>("nom"));
        prenomColumn.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        adresseColumn.setCellValueFactory(new PropertyValueFactory<>("adresse"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
        cinColumn.setCellValueFactory(new PropertyValueFactory<>("cin"));
        telColumn.setCellValueFactory(new PropertyValueFactory<>("telephone"));

        clickedButton = (Button) e.getSource();
        System.out.println(clickedButton.getUserData());
        Departement dept = dbConnect.getDepartementById((int) clickedButton.getUserData());
        System.out.println(dept.getNom());
        nomField2.setText(dept.getNom());
        ObservableList<Professeur> departementProfesseurs = dbConnect.getDepartementProfessors(dept);
        tableView.setItems(departementProfesseurs);
    }

    @FXML
    public void modifierDepart(ActionEvent event){
        System.out.println("modifier ...");
        Departement depart = new Departement();
        depart.setNom(nomField2.getText());

        Departement d = dbConnect.getDepartementById((int) clickedButton.getUserData());
        if(d.getNom().equals(depart.getNom())){
        }
        else{
            dbConnect.modifyDepartement(d, depart);
            System.out.println("modified");
        }
    }

    @FXML
    public void supprimerDepart(ActionEvent event){
        System.out.println("delete");
        Departement depart = dbConnect.getDepartementById((int) clickedButton.getUserData());
        dbConnect.removeDepartement(depart);
    }


    public static final LocalDate LOCAL_DATE (String dateString){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }

}
