package Traitements;

import javafx.collections.ObservableList;

import java.util.List;

public interface IMetier {
    //------- Professeurs -------//
    List<Professeur> getAllProfessors();
    List<Professeur> getProfessorsByMC(String motCle);
    void addProfessor(Professeur pr, int idDept);
    Professeur getProfessorsById(String id);
    void removeProfessor(Professeur pr);
    void modifyProfessor(Professeur pr, Professeur prUpdate);
    void addProfToDepart(Professeur pr, Departement depart);

    //------- Departement --------//
    List<Departement> getAllDepartements();
    Departement getDepartementById(int id);
    void addDepartement(Departement depart);
    void removeDepartement(Departement depart);
    void modifyDepartement(Departement depart, Departement departUpdate);
    void removePrFromDepart(Professeur pr);
    ObservableList<Professeur> getDepartementProfessors(Departement depart);

}
