package Traitements;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Departement implements Serializable {
    private int id_depart;
    private String nom;

    private List<Professeur> professeurs;

    public Departement(String nom, List<Professeur> professeurs) {
        this.nom = nom;
        this.professeurs = professeurs;
    }

    public Departement(String nom, int id) {
        this.nom = nom;
        this.id_depart = id;
        this.professeurs = new ArrayList<>();
    }

    public Departement() {
        this.professeurs = new ArrayList<>();
    }

    public void setId_depart(int id_depart) {
        this.id_depart = id_depart;
    }

    public int getId_depart() {
        return id_depart;
    }

    public String getNom() {
        return nom;
    }

    public List<Professeur> getProfesseurs() {
        return professeurs;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setProfesseurs(List<Professeur> professeurs) {
        this.professeurs = professeurs;
    }

    public void addProfessor(Professeur pr){
        this.professeurs.add(pr);
    }

}
