package Traitements;

import java.io.Serializable;
import java.util.Date;

public class Professeur implements Serializable {
    private int id_prof;
    private String nom;
    private String prenom;
    private String cin;
    private String adresse;
    private String telephone;
    private String email;
    private String date_recrutement;

    private Departement departement;

    public Professeur(String nom, String prenom, String cin, String adresse, String telephone, String email, String date_recrutement, Departement departement) {
        this.nom = nom;
        this.prenom = prenom;
        this.cin = cin;
        this.adresse = adresse;
        this.telephone = telephone;
        this.email = email;
        this.date_recrutement = date_recrutement;
        this.departement = departement;
    }

    public Professeur(String nom, String prenom, String cin, String adresse, String telephone, String email, String date_recrutement) {
        this.nom = nom;
        this.prenom = prenom;
        this.cin = cin;
        this.adresse = adresse;
        this.telephone = telephone;
        this.email = email;
        this.date_recrutement = date_recrutement;
    }

    public Professeur() {}

    public void setId_prof(int id_prof) {
        this.id_prof = id_prof;
    }

    public int getId_prof() {
        return id_prof;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getCin() {
        return cin;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getEmail() {
        return email;
    }

    public String getDate_recrutement() {
        return date_recrutement;
    }

    public Departement getDepartement() {
        return departement;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDate_recrutement(String date_recrutement) {
        this.date_recrutement = date_recrutement;
    }

    public void setDepartement(Departement departement) {
        this.departement = departement;
    }
}
