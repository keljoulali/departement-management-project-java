package Traitements;

import DAO.ConnectionDB;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MetierImpl implements IMetier{

    @Override
    public ObservableList<Professeur> getAllProfessors() {
        ObservableList<Professeur> professeurs = FXCollections.observableArrayList();
        Connection connexion= ConnectionDB.getConnection();
        try{
            PreparedStatement pstm = connexion.prepareStatement("select * from professeur;");
            ResultSet result = pstm.executeQuery();
            List<Departement> departements = getAllDepartements();
            Departement dept= new Departement();
            while(result.next()){
                for(Departement d:departements){
                    if(d.getId_depart() == result.getInt("departement")){
                        dept = d;
                    }
                }
                Professeur p = new Professeur(result.getString("Nom"),result.getString("Prenom"),result.getString("CIN"),result.getString("Adresse"),result.getString("Telephone"),result.getString("Email"),result.getString("Date_recrutement"),dept);
                professeurs.add(p);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return professeurs;
    }

    @Override
    public Professeur getProfessorsById(String id) {
        Connection connection = ConnectionDB.getConnection();
        Professeur pr = new Professeur();
        List<Departement> departements = getAllDepartements();
        Departement dept= new Departement();
        try{
            PreparedStatement pstm = connection.prepareStatement("select * from professeur where CIN=?;");
            pstm.setString(1,id);
            ResultSet result = pstm.executeQuery();
            while(result.next()){
                for(Departement d:departements){
                    if(d.getId_depart() == result.getInt("departement")){
                        dept = d;
                    }
                }
                pr = new Professeur(result.getString("Nom"),result.getString("Prenom"),result.getString("CIN"),result.getString("Adresse"),result.getString("Telephone"),result.getString("Email"),result.getString("Date_recrutement"),dept);
                pr.setId_prof(result.getInt("id_Pr"));
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        return pr;
    }

    @Override
    public ObservableList<Professeur> getProfessorsByMC(String motCle) {
        Connection connection = ConnectionDB.getConnection();
        ObservableList<Professeur> professeurs = FXCollections.observableArrayList();
        List<Departement> departements = getAllDepartements();
        Departement dept= new Departement();
        try{
            PreparedStatement pstm = connection.prepareStatement("select * from professeur where Nom like ?");
            pstm.setString(1,"%"+motCle+"%");
            ResultSet result = pstm.executeQuery();
            while(result.next()){
                for(Departement d:departements){
                    if(d.getId_depart() == result.getInt("departement")){
                        dept = d;
                    }
                }
                Professeur p = new Professeur(result.getString("Nom"),result.getString("Prenom"),result.getString("CIN"),result.getString("Adresse"),result.getString("Telephone"),result.getString("Email"),result.getString("Date_recrutement"),dept);
                professeurs.add(p);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return professeurs;
    }

    @Override
    public void addProfessor(Professeur pr, int idDept) {
        Connection conn=ConnectionDB.getConnection();
        try {
            PreparedStatement pstm=conn.prepareStatement("insert into professeur(Nom,Prenom,Adresse,Email,Telephone, CIN, date_recrutement, Departement) values (?,?,?,?,?,?,?,?);");
            pstm.setString(1,pr.getNom());
            pstm.setString(2,pr.getPrenom());
            pstm.setString(3,pr.getAdresse());
            pstm.setString(4,pr.getEmail());
            pstm.setString(5,pr.getTelephone());
            pstm.setString(6,pr.getCin());
            pstm.setString(7,pr.getDate_recrutement());
            pstm.setInt(8,idDept);

            pstm.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void removeProfessor(Professeur pr) {
        Connection conn = ConnectionDB.getConnection();
        try {
            PreparedStatement pstm = conn.prepareStatement("delete from professeur where id_Pr=?;");
            pstm.setInt(1, pr.getId_prof());
            pstm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void modifyProfessor(Professeur pr, Professeur prUpdate) {
        Connection conn=ConnectionDB.getConnection();
        try {
            PreparedStatement pstm=conn.prepareStatement("update professeur set Nom = ?,Prenom= ?,Adresse= ?,Email= ?,Telephone= ?, CIN= ?, date_recrutement= ?, Departement= ? where id_Pr=?;");
            pstm.setString(1,prUpdate.getNom());
            pstm.setString(2,prUpdate.getPrenom());
            pstm.setString(3,prUpdate.getAdresse());
            pstm.setString(4,prUpdate.getEmail());
            pstm.setString(5,prUpdate.getTelephone());
            pstm.setString(6,prUpdate.getCin());
            pstm.setString(7,prUpdate.getDate_recrutement());
            pstm.setInt(8,prUpdate.getDepartement().getId_depart());
            pstm.setInt(9, pr.getId_prof());
            pstm.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void addProfToDepart(Professeur pr, Departement depart) {
        depart.addProfessor(pr);
    }

    @Override
    public ObservableList<Departement> getAllDepartements() {
        ObservableList<Departement> departements = FXCollections.observableArrayList();
        Connection connexion= ConnectionDB.getConnection();
        try{
            PreparedStatement pstm = connexion.prepareStatement("select * from departement;");
            ResultSet result = pstm.executeQuery();
            while(result.next()){
                Departement p = new Departement(result.getString("NomDepart"), result.getInt("id_Depart"));
                departements.add(p);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return departements;

    }

    @Override
    public Departement getDepartementById(int id) {
        Connection connection = ConnectionDB.getConnection();
        Departement depart = new Departement();
        List<Departement> departements = getAllDepartements();
        try{
            PreparedStatement pstm = connection.prepareStatement("select * from departement where id_Depart=?;");
            pstm.setInt(1,id);
            ResultSet result = pstm.executeQuery();
            while(result.next()){
                depart = new Departement(result.getString("NomDepart"),result.getInt("id_Depart"));
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        return depart;
    }

    @Override
    public void addDepartement(Departement depart) {
        Connection conn = ConnectionDB.getConnection();
        try {
            PreparedStatement pstm = conn.prepareStatement("insert into departement(NomDepart) values (?);");
            pstm.setString(1, depart.getNom());
            pstm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

        @Override
    public void removeDepartement(Departement depart) {
            Connection conn = ConnectionDB.getConnection();
            try {
                PreparedStatement pstm = conn.prepareStatement("delete from departement where id_Depart=?;");
                pstm.setInt(1, depart.getId_depart());
                pstm.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
    }

    @Override
    public void modifyDepartement(Departement depart, Departement departUpdate) {
        Connection conn=ConnectionDB.getConnection();
        try {
            PreparedStatement pstm=conn.prepareStatement("update departement set NomDepart = ? where id_Depart=?;");
            pstm.setString(1,departUpdate.getNom());
            pstm.setInt(2, depart.getId_depart());
            pstm.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void removePrFromDepart(Professeur pr) {
        Professeur p = pr;
        p.setDepartement(null);
        modifyProfessor(pr, p);
    }

    @Override
    public ObservableList<Professeur> getDepartementProfessors(Departement depart) {
        Connection conn = ConnectionDB.getConnection();
        ObservableList<Professeur> profs = FXCollections.observableArrayList();
        try {
            PreparedStatement pstm = conn.prepareStatement("select * from professeur where Departement=?;");
            pstm.setInt(1, depart.getId_depart());
            ResultSet result = pstm.executeQuery();
            while(result.next()){
                Professeur p = new Professeur(result.getString("Nom"),result.getString("Prenom"),result.getString("CIN"),result.getString("Adresse"),result.getString("Telephone"),result.getString("Email"),result.getString("Date_recrutement"));
                profs.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return profs;
    }
}
