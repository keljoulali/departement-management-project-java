package Traitements;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

import java.io.FileNotFoundException;
import java.net.URL;

public class FxmlLoader {
    private AnchorPane view;

    public AnchorPane getPane(String fileName){
        try{
            URL fileUrl = getClass().getResource("../Presentation/"+fileName+".fxml");
            if(fileUrl==null){
                throw new java.io.FileNotFoundException("FXML file not found");
            }
            FXMLLoader loader = new FXMLLoader();
            view = loader.load(fileUrl);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }
}
