package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB {
    private static Connection connection;

    static {
        String db_name = "db_gestionecole";
        String db_user = "root";
        String db_password = "";
        String url = "jdbc:mysql://localhost:3306/" + db_name;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(url, db_user, db_password);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public static Connection getConnection() {
        return connection;
    }
}
